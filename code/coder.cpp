#include "coder.hpp"
#include <algorithm>
#include <random>
#include <set>
#include <chrono>

Coder::Coder()
{
}

Coder::Coder(double rate, int length, int weight)
    : m_rate {rate},
      m_length {length},
      m_weight {weight}
{
}

std::valarray<int> Coder::encode(std::valarray<int> &message)
{
    if (m_needGenerateCode)
        generateCode();

    message = std::valarray<int>(message[std::slice(0, m_code[0].size(), 1)]);
    std::valarray<int> result(0, m_code.size());
    for (size_t i = 0; i < m_code.size(); ++i)
    {
        result[i] = (message * m_code[i]).sum() % 2;
    }

    return result;
}

std::pair<std::valarray<bool>, int> Coder::decode(const std::valarray<double> &message,
                                                 int maxsteps)
{
    auto weight = m_rowIndices[0].size();

    std::vector<std::valarray<double>> z(m_rowIndices.size(), std::valarray<double>(0.0, message.size()));
    std::vector<int> p;
    std::valarray<double> y(weight);
    std::valarray<double> u;

    std::valarray<double> soft = message;
    std::valarray<bool> hard(message.size());
    hard = message > 0.5;
    if (!check_syndrome(hard)) return std::make_pair<std::valarray<bool>, int>(std::move(hard), 0);

    for (int step = 0; step < maxsteps; ++step)
    {
        for (size_t i = 0; i < m_rowIndices.size(); ++i)
        {
            p.reserve(weight);

            for (size_t j = 0; j < weight; ++j)
            {
                int elem = m_rowIndices[i][j] - 1;
                if (elem >= 0)
                {
                    p.push_back(elem);
                    y[j] = (soft[elem] - z[i][elem]);
                }
            }


            u = map(y);

            for (size_t j = 0; j < p.size(); ++j)
            {
                soft[p[j]] = u[j] + y[j];
                z[i][p[j]] = u[j];
            }

            p.clear();
            y = y.shift(u.size());
        }

        hard = message > 0.5;
        if (!check_syndrome(hard)) return std::make_pair<std::valarray<bool>, int>(std::move(hard), std::move(step));
    }

    return std::make_pair<std::valarray<bool>, int>(std::move(hard), -maxsteps);
}

void Coder::generateCode()
{
    int row_weight = m_weight / (1 - m_rate);
    int width = m_length / row_weight;
    int length = row_weight * width;
    std::vector<std::valarray<bool>> generateMatrix(m_weight * width, std::valarray<bool>(length));

    std::valarray<bool> tmp(false, length);
    std::fill_n(std::begin(tmp), row_weight, true);

    for (auto vec = generateMatrix.begin(); vec != std::next(generateMatrix.begin(), width); ++vec) {
        *vec = tmp;
        tmp = tmp.cshift(-row_weight);
    }

    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine engine(seed);

    std::vector<int> perm(length);
    std::iota(perm.begin(), perm.end(), 0);
    for (int k = 1; k < m_weight; ++k) {
        std::shuffle(perm.begin(), perm.end(), engine);

        for (int i = 0; i < width; ++i) {
            for (int j = 0; j < length; ++j) {
                generateMatrix[k * width + i][j] = generateMatrix[i][perm[j]];
            }
        }
    }

    m_columnIndices.resize(m_weight * width, std::vector<int>());
    m_rowIndices.resize(length, std::vector<int>());
    for (auto vec = generateMatrix.begin(); vec != generateMatrix.end(); ++vec) {
        auto it = std::begin(*vec);
        while ((it = std::find(it, std::end(*vec), true)) != std::end(*vec))
        {
            int row = std::distance(generateMatrix.begin(), vec);
            int col = std::distance(std::begin(*vec), it);
            m_columnIndices[row].push_back(col);
            m_rowIndices[col].push_back(row);
            ++it;
        }
    }

    m_code = findCheckMatrix(generateMatrix);
    m_needGenerateCode = false;
}

std::vector<std::valarray<int>> Coder::findCheckMatrix(std::vector<std::valarray<bool>> &generatorMatrix)
{
    std::set<int> inform_set;
    int col = 0;
    auto it = generatorMatrix.begin();
    while (it != generatorMatrix.end()) {
        while (it != generatorMatrix.end() && it->max() == false)
        {
            it = generatorMatrix.erase(it);
        }

        auto row = it;
        for (; row != generatorMatrix.end(); ++row) {
            if ((*row)[col] == 1)
                break;
        }

        if (row != generatorMatrix.end())
        {
            inform_set.insert(col);
            for (auto iter = generatorMatrix.begin(); iter != generatorMatrix.end(); ++iter)
            {
                if (iter != row && (*iter)[col] == 1)
                    *iter = *row ^ *iter;
            }
            it->swap(*row);
            ++it;
        }

        ++col;
    }

    int row_num = generatorMatrix[0].size() - generatorMatrix.size();
    std::vector<int> check_set(row_num);
    std::iota(check_set.begin(), check_set.end(), 0);
    std::set_difference(check_set.begin(), check_set.end(), inform_set.begin(), inform_set.end(), check_set.begin());

    std::vector<std::valarray<int>> P(generatorMatrix.size(), std::valarray<int>(0, row_num));
    for (size_t i = 0; i < P.size(); ++i)
    {
        for (int j = 0; j < row_num; ++j) {
            P[i][j] = generatorMatrix[i][check_set[j]];
        }
    }

    auto row = 0;
    col = 0;
    std::vector<std::valarray<int>> result(generatorMatrix[0].size(), std::valarray<int>(0, row_num));
    for (auto &vec : result)
    {
        if (auto iter = inform_set.find(row);
                iter != inform_set.end())
        {
            vec.swap(P.at(std::distance(inform_set.begin(), iter)));
        }
        else
        {
            vec[vec.size() - 1 - col] = true;
            ++col;
        }
        ++row;
    }

    return result;
}

bool Coder::check_syndrome(const std::valarray<bool>& hard)
{
    int count = 0;
    for (auto it = m_rowIndices.begin(); it != m_rowIndices.end(); ++it)
    {
        for (const auto j : *it)
            if (j > 0)
                count += static_cast<int>(hard[j - 1]);
        if (count % 2)
            return true;
    }

    return false;
}

std::valarray<double> Coder::map(const std::valarray<double> &input)
{
    std::valarray<double> result = input;
    if (!(input == 0.0).min())
    {
        std::valarray<double> hard(input.size());
        for (size_t i = 0; i < hard.size(); ++i)
        {
            hard[i] = input[i] < 0 ? 1 : -1;
        }

        double count = std::count(std::begin(hard), std::end(hard), -1);

        std::valarray<double> logexps = logexp(input);

        double sum = logexps.sum();
        logexps -= sum;

        result = count * hard * logexp(logexps);
    }
    else
    {
        auto count = 0;
        double sum = 0;
        for (const auto elem : input)
        {
            if ((int)elem == 0)
                ++count;
            else if (elem < 0)
                sum += elem;
        }

        if (count == 1)
        {
            auto synd = fmod(sum, 2);

            double sumAlogpy = logexp(input).sum();

            auto newValue = (2 * synd - 1) * log(tanh(-sumAlogpy/2));
            std::replace_copy(std::begin(input), std::end(input), std::begin(result), 0., newValue);
        }
    }

    return result;
}

inline std::valarray<double> Coder::logexp(const std::valarray<double> &x)
{
    return x.apply([](double item){
        if ((int)item == 0) return 0.0;
        item = item < -16 ?
                    -16 :
                    item > 16 ?
                        16 :
                        item;
        return log(tanh(item / 2));
    });
}

void Coder::setSettings(double rate, int length, int weight)
{
    m_rate = rate;
    m_length = length;
    m_weight = weight;

    m_needGenerateCode = true;
}
