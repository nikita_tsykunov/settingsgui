#ifndef CODER_HPP
#define CODER_HPP

#include <vector>
#include <valarray>

class Coder
{
public:
    Coder();
    Coder(double rate, int length, int weight);

public:
    std::valarray<int> encode(std::valarray<int> &message);
    std::pair<std::valarray<bool>, int> decode(const std::valarray<double> &message, int maxsteps);

    void setSettings(double rate, int length, int weight);

private:
    void generateCode();
    std::vector<std::valarray<int>> findCheckMatrix(std::vector<std::valarray<bool>> &generatorMatrix);
    bool check_syndrome(const std::valarray<bool> &hard);
    std::valarray<double> map(const std::valarray<double>& input);
    inline std::valarray<double> logexp(const std::valarray<double> &x);

private:
    std::vector<std::valarray<int>> m_code;
    std::vector<std::vector<int>> m_columnIndices;
    std::vector<std::vector<int>> m_rowIndices;
    double m_rate = 0.5;
    int m_length = 100;
    int m_weight = 3;
    bool m_needGenerateCode = true;
};

#endif // CODER_HPP
