#include "modulator.hpp"
#include <valarray>


Modulator::Modulator()
{
}

std::valarray<int> Modulator::modulate(const std::valarray<int> &message) const
{
    std::valarray<int> result(message.size() / 2);

    auto elem = 0;
    for (auto it = std::begin(message); it != std::end(message) && it != std::prev(std::end(message)); it += 2, ++elem)
    {
        auto num = std::find_if(m_transformDict.begin(),
            m_transformDict.end(),
            [&it](auto elem) {return elem.second == std::make_pair(*it, *(std::next(it))); });
        result[elem] = (num->first);
    }

    return result;
}

std::valarray<double> Modulator::demodulate(const std::valarray<double>& sequence, double sigma_squared) const
{
    std::valarray<double> probs(4);
    auto probability = [this, sigma_squared](double num, std::valarray<double> & probs)
    {
        std::transform(
            m_transformDict.begin(),
            m_transformDict.end(),
            std::begin(probs),
            [sigma_squared, num](auto elem) {return std::exp(-std::pow(elem.first - num, 2) / sigma_squared); }
        );

        probs /= probs.sum();
        return std::make_pair(probs[2] + probs[3], probs[1] + probs[2]);
    };

    std::valarray<double> result(sequence.size() * 2);
    size_t num = 0;
    for (const auto item : sequence)
    {
        auto pair = probability(item, probs);

        result[num] = pair.first;
        result[++num] = pair.second;
        ++num;
    }

    return result;
}
