#pragma once

#include <map>
#include <QObject>

class Modulator : public QObject
{
    Q_OBJECT

public:
    Modulator();
    ~Modulator() = default;

    std::valarray<int> modulate(const std::valarray<int> &message) const;
    std::valarray<double> demodulate(const std::valarray<double> &sequence, double sigma_squared) const;

private:
    const std::map<int, std::pair<int, int>> m_transformDict{
        {-3, {0, 0}},
        {-1, {0, 1}},
        { 1, {1, 1}},
        { 3, {1, 0}}
    };
};

