#include <random>
#include <functional>
#include <QTime>
#include <QDebug>

#include "runner.hpp"
#include "utils.hpp"

Runner::Runner(const std::shared_ptr<QSettings> &settings)
    : m_settings {settings}
{
    {
        m_settings->beginGroup("code");
        QString rate = m_settings->value("rate").toString();
        auto tokens = rate.split("/", QString::SplitBehavior::SkipEmptyParts);

        m_coder = std::make_unique<Coder>(tokens[0].toDouble() / tokens[1].toDouble(),
                  m_settings->value("length").toInt(),
                  m_settings->value("column_weight").toInt());
        m_settings->endGroup();
    }
    {
        m_settings->beginGroup("signal_to_noise_ratio");
        std::tuple<double, double, double> snr(
            m_settings->value("start").toDouble(),
            m_settings->value("stop").toDouble(),
            m_settings->value("step").toDouble()
        );
        m_settings->endGroup();

        m_modulator = std::make_unique<Modulator>();
    }
}


void Runner::run()
{
    if (m_message.size() == 0)
        generateMessage();

    auto encodedMessage = m_coder->encode(m_message);
    auto modulatedMessage = m_modulator->modulate(encodedMessage);

    const auto normFactor = 2 * (m_settings->value("QAM-scheme").toInt() - 1) / 3;
    auto sigma = std::sqrt(normFactor * std::pow(10, -( m_settings->value("signal_to_noise_ratio/start").toDouble()) / 10) / 2);

    double errors = 0;
    for (int i = 0; i < m_settings->value("number_of_cycles").toInt(); ++i)
    {
        emit valueChanged(i);
        auto noisedMessage = Utils::addNoise(modulatedMessage, sigma);

        auto demodulatedMessage = m_modulator->demodulate(noisedMessage, 2 * std::pow(sigma, 2));
        auto decodedMessage = m_coder->decode(demodulatedMessage, 50);

        auto hard = std::get<0>(decodedMessage);
        std::valarray<int> int_hard(hard.size());

        std::transform(std::begin(hard), std::end(hard), std::begin(int_hard), [](auto item){return static_cast<int>(item); });
        hard = int_hard != encodedMessage;
        errors += std::count(std::begin(hard), std::end(hard), true);
    }

    emit finished(QString::number(errors / m_settings->value("number_of_cycles").toInt() / encodedMessage.size()));
}

void Runner::setCoderSettings()
{
    m_settings->beginGroup("code");
    QString rate = m_settings->value("rate").toString();
    auto tokens = rate.split("/", QString::SplitBehavior::SkipEmptyParts);

    m_coder->setSettings(tokens[0].toDouble() / tokens[1].toDouble(),
              m_settings->value("length").toInt(),
              m_settings->value("column_weight").toInt());

    m_settings->endGroup();
}

void Runner::generateMessage()
{
    m_message = Utils::generateMessage(m_settings->value("code/length").toInt());

    emit messageGenerated(QTime::currentTime().toString("HH.mm.ss"));
}
