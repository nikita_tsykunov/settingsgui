#ifndef RUNNER_HPP
#define RUNNER_HPP

#include <QObject>
#include <QSettings>
#include <memory>
#include <valarray>
#include "coder.hpp"
#include "modulator.hpp"

class Runner : public QObject
{
    Q_OBJECT

signals:
    void finished(QString message);
    void valueChanged(int i);
    void messageGenerated(QString hash);

public:
    Runner(const std::shared_ptr<QSettings> &settings);

    void setCoderSettings();

public slots:
    void run();
    void generateMessage();

private:
    std::shared_ptr<QSettings> m_settings = nullptr;
    std::unique_ptr<Modulator> m_modulator = nullptr;
    std::unique_ptr<Coder> m_coder = nullptr;
    std::valarray<int> m_message;
};

#endif // RUNNER_HPP
