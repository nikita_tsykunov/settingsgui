#include <chrono>
#include <random>
#include <QDebug>

#include "utils.hpp"

Utils::Utils()
{
}

std::valarray<int> Utils::generateMessage(int length)
{
    std::valarray<int> result(length);

    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine engine(seed);
    std::bernoulli_distribution distribution;

    auto gen = [&distribution, &engine]() {return distribution(engine); };

    std::generate(std::begin(result), std::end(result), gen);

    return result;
}

std::valarray<double> Utils::addNoise(const std::valarray<int>& sequence, double sigma)
{
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine engine(seed);
    std::normal_distribution<double> distribution(0., sigma);

    std::valarray<double> result(sequence.size());
    std::transform(
        std::begin(sequence),
        std::end(sequence),
        std::begin(result),
        [&distribution, &engine](auto elem) {return elem + distribution(engine); }
    );

    return result;
}
