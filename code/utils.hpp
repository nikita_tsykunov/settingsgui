#ifndef UTILS_HPP
#define UTILS_HPP

#include <valarray>

class Utils
{
public:
    Utils();
public:
    static std::valarray<int> generateMessage(int length);
    static std::valarray<double> addNoise(const std::valarray<int>& sequence, double sigma);
};

#endif // UTILS_HPP
