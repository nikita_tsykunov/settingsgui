#include "mainwindow.hpp"
#include "code/coder.hpp"

MainWindow::MainWindow(QWidget* parent)
    : QDialog(parent)
{
    m_settings = std::make_shared<QSettings>(QApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat);
    m_runner = std::make_unique<Runner>(m_settings);

    createSettings();
    createThread();
}


void MainWindow::createThread()
{
    runnerThread = new QThread();
    m_runner->moveToThread(runnerThread);
    runnerThread->start();
}

void MainWindow::createSettings()
{
    QVBoxLayout* mainLayout = new QVBoxLayout();
    this->setLayout(mainLayout);

    QGroupBox * generalBox = new QGroupBox("General");
    generalBox->setObjectName("General");
    generalBox->setLayout(new QFormLayout());
    mainLayout->addWidget(generalBox);

    for (auto key : m_settings->allKeys())
    {
        if (key.contains("/"))
        {
            auto tokens = key.split("/", QString::SplitBehavior::SkipEmptyParts);
            for (auto &token : tokens)
            {
                token.replace("_", " ").replace(0, 1, token[0].toTitleCase());
            }

            QGroupBox *widget = this->findChild<QGroupBox *>(tokens[0], Qt::FindDirectChildrenOnly);
            if (widget == nullptr)
            {
                widget = new QGroupBox(tokens[0]);
                widget->setObjectName(tokens[0]);
                widget->setLayout(new QFormLayout());

                mainLayout->addWidget(widget);
            }

            QLineEdit* edit = new QLineEdit(m_settings->value(key, 0.0).toString());
            edit->setObjectName(key);
            connect(edit, &QLineEdit::editingFinished, this, &MainWindow::saveSetting);

            qobject_cast<QFormLayout *>(widget->layout())->addRow(new QLabel(tokens[1]), edit);
        }
        else
        {
            QLineEdit* edit = new QLineEdit(m_settings->value(key, 0.0).toString());
            edit->setObjectName(key);
            connect(edit, &QLineEdit::editingFinished, this, &MainWindow::saveSetting);

            key.replace("_", " ").replace(0, 1, key[0].toTitleCase());
            qobject_cast<QFormLayout *>(generalBox->layout())->addRow(new QLabel(key), edit);
        }
    }

    addMessageGroupBox(mainLayout);

    m_result = std::make_unique<QLabel>("Result appears here");
    connect(m_runner.get(), &Runner::finished, m_result.get(), &QLabel::setText);
    mainLayout->addWidget(m_result.get());

    m_progressBar = std::make_unique<QProgressBar>();
    m_progressBar->setMaximum(m_settings->value("number_of_cycles").toInt() - 1);
    connect(m_runner.get(), &Runner::valueChanged, m_progressBar.get(), &QProgressBar::setValue);
    mainLayout->addWidget(m_progressBar.get());

    runButton = new QPushButton("Run");
    connect(runButton, &QPushButton::released, m_runner.get(), &Runner::run);

    mainLayout->addWidget(runButton);
}

void MainWindow::saveSetting()
{
    QLineEdit* edit = qobject_cast<QLineEdit*>(sender());

    m_settings->setValue(edit->objectName(), edit->text());

    if (edit->objectName().contains("number_of_cycles"))
    {
        m_progressBar.reset();
        m_progressBar->setMaximum(m_settings->value("number_of_cycles").toInt() - 1);
    }
    if (edit->objectName().contains("code/"))
    {
        m_runner->setCoderSettings();
    }

}

void MainWindow::addMessageGroupBox(QVBoxLayout *layout)
{
    QGroupBox* messageBox = new QGroupBox("Message");
    QFormLayout* messageLayout = new QFormLayout();

    QLineEdit* edit = new QLineEdit();
    edit->setReadOnly(true);
    connect(m_runner.get(), &Runner::messageGenerated, edit, &QLineEdit::setText);
    messageLayout->addRow(new QLabel("Message time"), edit);

    QPushButton* button = new QPushButton("Generate");
    connect(button, &QPushButton::released, m_runner.get(), &Runner::generateMessage);
    messageLayout->addWidget(button);

    messageBox->setLayout(messageLayout);
    layout->addWidget(messageBox);
}

