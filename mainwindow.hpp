#pragma once

#include <QtWidgets>
#include <QSettings>
#include <QLabel>
#include <memory>
#include "code/runner.hpp"

class QGroupBox;
class QLabel;
class QLineEdit;

class MainWindow : public QDialog
{
    Q_OBJECT

public:
    MainWindow(QWidget* parent = Q_NULLPTR);

private slots:
    void saveSetting();

private:
    void createSettings();
    void createThread();
    void addMessageGroupBox(QVBoxLayout *layout);

private:
    std::shared_ptr<QSettings> m_settings = nullptr;
    QPushButton* runButton;
    std::unique_ptr<QLabel> m_result = nullptr;
    std::unique_ptr<QProgressBar> m_progressBar = nullptr;

    std::unique_ptr<Runner> m_runner = nullptr;
    QThread* runnerThread = nullptr;
};
